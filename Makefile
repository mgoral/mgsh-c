include config.mk

SRC = src/main.c src/vector.c src/commands.c src/error.c src/parser.c
OBJ = $(subst .c,.o,$(SRC))

all: mgsh

mgsh: $(OBJ)
	@echo CC $@
	@$(CC) -o $@ $(OBJ) $(LDFLAGS)

%.o: %.c
	@echo CC $<
	@$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(RM) $(OBJ)
