INC = -I -I/usr/include
LIB = -L/usr/lib -lc -lm

CFLAGS += -g -Wall -Werror -std=c11 -pedantic -O2 $(INC)
LDFLAGS += -g $(LIB)

# some programs
CC = gcc
RM = rm -f
