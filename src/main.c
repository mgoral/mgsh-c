/**
 * Copyright (C) 2016 Michał Góral.
 * 
 * This file is part of mgsh
 * 
 * mgsh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * mgsh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mgsh. If not, see <http://www.gnu.org/licenses/>.
 */

#define _POSIX_C_SOURCE 200809L

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>

#include "commands.h"
#include "parser.h"
#include "vector.h"
#include "error.h"

const char *ps()
{
    return "> ";
}

char *readline()
{
    size_t bufsize = 256;
    char *line = (char *)malloc(bufsize);
    if (getline(&line, &bufsize, stdin) < 0)
        line = NULL;
    return line;
}

void run(void)
{
    string_vector args;
    vector_init(&args);

    while (true) {
        printf("%s %s", getcwd(NULL, 0), ps());
        char *line = readline();

        command_func_t func;
        parse(line, &func, &args);
        if (func(args.data))
        {
            last_error();
        }
        vector_clear(&args);
    }

    vector_free(&args);
}

int main(int argc, char **argv)
{
    run();
    return 0;
}
