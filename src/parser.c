/**
 * Copyright (C) 2016 Michał Góral.
 * 
 * This file is part of mgsh
 * 
 * mgsh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * mgsh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mgsh. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>

#include "parser.h"

struct TokenPair
{
    const char *name;
    command_func_t func;
};

struct TokenPair mapping[] = {
    {"cd", &cmd_cd},
    {"exit", &cmd_exit},
};

static command_func_t find_func(const char* name)
{
    size_t mapping_size = sizeof(mapping) / sizeof(struct TokenPair);
    for (size_t i = 0; i < mapping_size; ++i) {
        if (!strcmp(name, mapping[i].name)) {
            return mapping[i].func;
        }
    }
    return NULL;
}

static char *token(char *str, const char *delim)
{
    return strtok(str, delim);
}

void parse(char *line, command_func_t *func, string_vector* args)
{
    const char *delim = " \n\r";
    char *tok = token(line, delim);
    command_func_t found = find_func(tok);
    if (!found) {
        *func = &cmd_exec;
        vector_push(args, tok);
    }
    else {
        *func = found;
    }

    while ((tok = token(NULL, delim)))
        vector_push(args, tok);
}
