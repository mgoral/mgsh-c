/**
 * Copyright (C) 2016 Michał Góral.
 * 
 * This file is part of mgsh
 * 
 * mgsh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * mgsh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mgsh. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#include "vector.h"

void vector_init_impl(vector_info *info, size_t data_size)
{
    info->buf = NULL;
    info->capacity = 0;
    info->size = 0;
    *(size_t *)&info->elem_size = data_size;  // cast away constness for a while
    vector_reserve_impl(info, 4);
}

void vector_free_impl(vector_info *info)
{
    free(info->buf);
    info->buf = NULL;
    info->capacity = 0;
    info->size = 0;
}

void vector_reserve_impl(vector_info *info, size_t new_capa)
{
    if (new_capa <= info->capacity)
        return;

    size_t new_size = info->elem_size * new_capa * 2;
    void* new_data = realloc(info->buf, new_size);
    if (!new_data)
        exit(EXIT_FAILURE);  // TODO: can we handle this gracefully?
    info->buf = new_data;
    info->capacity = new_capa;
}

//void* vector_pop_impl(void *data, vector_info *info)
//{
//    if (info->size == 0)
//        return NULL;
//    return data + info->elem_size * --info->size;
//}
