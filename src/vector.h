/**
 * Copyright (C) 2016 Michał Góral.
 * 
 * This file is part of mgsh
 * 
 * mgsh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * mgsh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with mgsh. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VECTOR_H_
#define VECTOR_H_

#include <stddef.h>

typedef struct {
    size_t size;
    size_t capacity;
    const size_t elem_size;
    void *buf;
} vector_info;

#define vector_t(T) \
struct {\
    T *data;\
    vector_info info;\
}

/**
 * Initializes vector.
 */
void vector_init_impl(vector_info *info, size_t data_size);
#define vector_init(vec) \
        (vector_init_impl(&(vec)->info, sizeof((vec)->data)), \
         (vec)->data = (vec)->info.buf)

/**
 * Removes a vector from memory. It's unusable after this call.
 */
void vector_free_impl(vector_info *info);
#define vector_free(vec) vector_free_impl(&(vec)->info)

/**
 * Reserves memory for `new_capa` number of objects. No action is performed if
 * `new_capa is smaller than vector's current capacity.
 */
void vector_reserve_impl(vector_info *info, size_t new_capa);
#define vector_reserve(vec, new_capa) \
        vector_reserve_impl(&(vec)->info, new_capa)

/**
 * Copies a value into initialized vector.
 * If data's capacity would be exceeded, it's resized to fit the new element. It
 * can cause reallocation of the underlying array.
 */
#define vector_push(vec, val) \
        do { \
            if ((vec)->info.capacity == (vec)->info.size) \
                vector_reserve((vec), (vec)->info.capacity * 2); \
            (vec)->data[(vec)->info.size++] = val; \
        } while (0)

/**
 * Removes and copies the last element to the pointer ret.
 */
#define vector_pop(vec, ret) \
        do { \
            memcpy((void *)ret, (vec)->data[(vec)->info.size - 1], \
                   (vec)->info.elem_size); \
            (vec)->info.size ? --(vec)->info.size : 0; \
        } while (0)

/**
 * Clears the contents of a given vector
 */
#define vector_clear(vec) (vec)->info.size = 0

#endif
